<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php 
        include 'include/myFunction.php';
        autoInclude();
    ?>
    <title>Welcome</title>
</head>
<body>
    <?php 
        showErrMsg();
        if (isset($_POST["message"])){ 
            $message = "<b>Logged out successfully.</b>";
            $btn = createButton(["id" => "btnCloseNoti", "class"=> "btn"], ["class"=> "btnWrapper"]);
            echo myDiv("div", $btn.$message , ["class" => "alert success", "id"=> "notification-bar"]);
        }
    ?>
    <h2>Welcome to POS SYSTEM for SAMTTAJ Bookshop</h2>
    <br> <br>
    <button onclick="location.href='<?php echo posRoot . "home/login.php";  ?>'">Login</button> <br>
    <button onclick="location.href='<?php echo posRoot . "home/logout.php";  ?>'">Logout</button>
    <div id="notification-bar" ></div>
    <div>
    </div>
</body>
</html>