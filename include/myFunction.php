<?php

declare(strict_types=1);

//prevent direct acess from url
function noDirectAccess()
{
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && realpath(__FILE__) == realpath($_SERVER['SCRIPT_FILENAME'])) {
        die(header('location: /pos/error.php?err=403'));
    }
}
noDirectAccess();
//define root path
define("posRoot", "/pos/");

/**
 * return a div element in a string
 * @param string $type required || e.g. div , input
 * @param string $content required || the thing that will be displayed by the div
 * @param array $otherAttribute optional || takes key value pair || use key "none" for attribute with no value
 * @param string $style optional || syle of the div
 */
function myDiv(string $type, string $content = null, array $otherAttribute = null, string $style = null): string
{
    $noClosingElement = ["input", "link"];
    $rtndiv = "<" . $type . " ";
    if (isset($style)) $rtndiv .= "style=\"" . $style . ";\" ";
    if (isset($otherAttribute)) {
        foreach ($otherAttribute as $key => $value) {
            if ($key != "none") $rtndiv .= "$key=\"" . $value . "\" ";
            else $rtndiv .= $value . " ";
        }
    }
    $rtndiv .= ">";
    if (!in_array($type, $noClosingElement)) $rtndiv .= $content . "</" . $type . ">";
    return $rtndiv;
}

/**
 * @param mysqli_result $sqlResult A result set identifier returned by mysqli_query(), mysqli_store_result() or mysqli_use_result().
 * @param string $colNameVal  column name of the value || case insensitive
 * @param string $colNameDisplay  column name of display text || case insensitive
 */
function genSelectOpt(mysqli_result $sqlResult, string $colNameVal, string $colNameDisplay): string
{
    if (!isset($rtnStr)) {
        $rtnStr = '';
        $colNameDisplay = strtolower($colNameDisplay);
        $colNameVal = strtolower($colNameVal);
    }
    while ($row = mysqli_fetch_assoc($sqlResult)) {
        $rtnStr .= "<option value=\"";
        $rtnStr .= $row[$colNameVal];
        $rtnStr .= "\">";
        $rtnStr .= strtoupper($row[$colNameDisplay]);
        $rtnStr .= "</option>";
    }
    return $rtnStr;
}


/**
 * trim input and perform htmlspecialchars
 */
function checkInput($userInput)
{
    $userInput = trim($userInput);
    $userInput = stripslashes($userInput);
    $userInput = htmlspecialchars($userInput);
    return $userInput;
}

function validatePassword(string $password): bool
{
    $rtn = false;
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    if ($uppercase && $lowercase && $number  && !strlen($password) < 8) {
        $rtn = true;
    }
    return $rtn;
}

function genRadioOption(array $optValue, string $radioName, string $labelClass, int $isRequired = 0): string
{
    $rtn = "";
    foreach ($optValue as $key) {
        //define attribute
        $radioAtt = ["type" => "radio", "name" => $radioName, "id" => $key, "value" => $key, "class" => "radio-group"];
        $labelAtt = ["for" => $key];
        if ($isRequired == 1) $radioAtt["none"] = "required";
        if (isset($labelClass)) $labelAtt["class"] = $labelClass;
        //create item
        $rtn .= myDiv("input", null, $radioAtt);
        $rtn .= myDiv("label", ucfirst($key), $labelAtt);
    }
    return $rtn;
}

function myGoToPage(string $destinationPath): string
{
    return "<script>location.href = \"$destinationPath\";</script>";
}

function createButton(array $btnAttributes = null, $wrapperAttributes = null, string $btntext = "<b>X</b>"): string
{
    $rtn = '';
    //create close button
    $createBtn = myDiv("button", $btntext, $btnAttributes);
    //wrap button
    $rtn = myDiv("div", $createBtn, $wrapperAttributes);
    return $rtn;
}

function uploadImage(string $basename, string $tmpname, int $insid)
{
    @include 'con_db.php';
    //set upload path
    $targetDir = "../../uploads/";
    //rename file to prevent overwriting
    $temp = explode(".", $basename);
    $newfilename = round(microtime(true)) . '.' . end($temp);
    $targetFilePath = $targetDir . $newfilename;
    //save file in server
    if (move_uploaded_file($tmpname, $targetFilePath)) {
        //insert path to db
        $pdsql = "update products set image = ? where pd_id = ?";
        $insertImage = mysqli_prepare($conn, $pdsql);
        mysqli_stmt_bind_param($insertImage, 'si', $newfilename, $insid);
        if (!mysqli_stmt_execute($insertImage)) {
            die("Error: " . $conn->error);
        }
    } else {
        die("Error: Image upload error");
    }
}

//impure function saperator
define("IMPURE_FUNCTION_BELOW", "IMPURE_FUNCTION_BELOW", true);
/**********   Impure functions (maybe)   **********/

function cleanPOSTData(){
    array_walk($_POST, function(&$value, $key) {
        $value = checkInput($value);
        $value = strtolower($value);
    });

}

/**
 *  auto include css and js from include folder + favicon
 * @param array $file path to the file from /pos/
 */
function autoInclude(array $file = null)
{
    //echo default include
    //open sans font
    echo "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
    <link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
    <link href=\"https://fonts.googleapis.com/css2?family=Open+Sans&display=swap\" rel=\"stylesheet\"> ";
    //favicon
    echo myDiv("link", null, ["rel" => "shortcut icon", "href" => posRoot . "favicon.ico", "type" => "image/x -icon"]);
    //css
    echo myDiv("link", null, ["rel" => "stylesheet", "href" => posRoot . "include/mystyle.css"]);
    //js
    echo myDiv("script", null, ["src" => posRoot . "include/myjsfx.js", "none" => "defer"]);
    //echo additional include
    if (isset($file)) {
        foreach ($file as $key => $value) {
            if ($key == "css")
                echo myDiv("link", null, ["rel" => "stylesheet", "href" => $value]);
            if ($key == "js")
                echo myDiv("script", null, ["src" => $value, "none" => "defer"]);
        }
    }
}

/**
 * set the error flag and message in $_SESSION array
 */
function setErrorFlag(string $errMsg, $type = "fail")
{
    @session_start();
    $_SESSION["errorFlag"] = 1;
    //wrap message and button
    $displayMsg = myDiv("div", $errMsg, null, "display:inline-block");
    $closeBtn = createButton(["id" => "btnCloseNoti", "class" => "btn"], ["class" => "btnWrapper"]);
    $rtnDiv = myDiv("div", $closeBtn . $displayMsg, ["class" => "alert " . $type, "id" => "notification-bar"]);
    $_SESSION["errMsg"] = $rtnDiv;
}

/**
 * display error message and clear flag
 */
function showErrMsg()
{
    @session_start();
    $_SESSION["errorFlag"] = null;
    if (isset($_SESSION["errMsg"])) {
        echo $_SESSION["errMsg"];
        $_SESSION["errMsg"] = null;
    }
}

/**
 * check if user is logged in and check if the role is true
 */
function myCheckSession(array $role = null)
{
    @session_start();
    showErrMsg();
    $kickTo = posRoot . "index.php";
    if (!isset($_SESSION["accid"], $_SESSION["emp_id"], $_SESSION["uName"], $_SESSION["role"])) {
        $message = "<b>You are not logged in!</b>";
        setErrorFlag(myDiv("div", $message));
        echo myGoToPage($kickTo);
    }
    if (isset($role) && !in_array($_SESSION["role"], $role)) {
        $message = "<b>Access denied! You do not have permission to access this content.</b>";
        setErrorFlag(myDiv("div", $message));
        echo myGoToPage($kickTo);
    }
    if ($_SESSION["userStatus"] == "deactivated") {
        $message = "<b>Your Account was deactivated. Please contact your administrator for more information.</b>";
        setErrorFlag(myDiv("div", $message));
        echo myGoToPage($kickTo);
    }
}

function handleShowEmployeeList()
{
    include 'con_db.php';
    $sql = "select emp_id, name, status, role from employees where emp_id != '$_SESSION[emp_id]'";
    // get all employee
    $getEmployee = mysqli_prepare($conn, $sql);
    mysqli_stmt_execute($getEmployee);
    $result = mysqli_stmt_get_result($getEmployee);
    while ($row = mysqli_fetch_assoc($result)) {
        $content = "<td>" . strtoupper($row["name"]) . "</td>";
        $content .= "<td>" . strtoupper($row["status"]) . "</td>";
        $content .= "<td>" . strtoupper($row["role"]) . "</td>";
        $content .= "<td>" . "<button class=\"btn btn-block btn-primary gotoempbtn\">Details</button>" . "</td";

    echo myDiv("tr", $content , ["id" => $row["emp_id"]]);
    }
}

function handleShowProductList()
{
    include 'con_db.php';
    $sql = "select p.pd_id, p.name, p.description, p.price, p.barcode, c.name as \"ctg\"
        from products p
        join pd_category c
        on p.pd_category = c.ctg_id";
    // get all employee
    $getEmployee = mysqli_prepare($conn, $sql);
    mysqli_stmt_execute($getEmployee);
    $result = mysqli_stmt_get_result($getEmployee);
    while ($row = mysqli_fetch_assoc($result)) {
        $content = "";
        $content .= "<td>" . strtoupper($row["name"]) . "</td>";
        $content .= "<td>" . strtoupper($row["description"]) . "</td>";
        $content .= "<td>" . ($row["price"]) . "</td>";
        $content .= "<td>" . ($row["barcode"]) . "</td>";
        $content .= "<td>" . strtoupper($row["ctg"]) . "</td>";
        $content .= "<td>" . "<button class=\"btn btn-block btn-primary gotopdbtn\">Details</button>" . "</td";
        echo myDiv("tr", $content, ["id" => $row["pd_id"]]);
    }
}

/**
 * @param string $EmpName name of the employee
 * @param string $email the email that will be used for the account
 * @param string $password the exact password entered by user
 */
function handleRegister(string $EmpName, string $email, string $password, string $role, string $manager)
{
    //check of email format is valid
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $rtnmess = "Entered email is invalid!";
        setErrorFlag($rtnmess);
        return;
    }
    //pre defining variables
    $type = "fail";
    $rtn = false;
    //check for empty variable
    if (!isset($EmpName, $email, $password, $role, $manager)) {
        $nullMessage = myDiv("b", "One or more field was not filled!");
        setErrorFlag(myDiv("div", $nullMessage));
        return $rtn;
    }
    //sanitize email
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    //validate email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailError = myDiv("b", "Email entered is not valid!");
        setErrorFlag(myDiv("div", $emailError));
    }
    include __DIR__ . "/con_db.php";
    // check for duplicate email
    $getEmail = mysqli_prepare($conn, "select * from accounts where email = ?");
    mysqli_stmt_bind_param($getEmail, 's', $email);
    mysqli_stmt_execute($getEmail);
    $result = mysqli_stmt_get_result($getEmail);
    $numrows = mysqli_num_rows($result);
    if ($numrows == 0) {
        //when no duplicate email found      
        if (!validatePassword($password)) {
            //alert user that password did not satisfy requirement
            $message = "<b>Password entered does not satisfy the requirement!</b>";
        } else {
            //when password fulfills requirements
            //encrypt password
            $enc_pass = $email . $password;
            $enc_pass = password_hash($enc_pass, PASSWORD_DEFAULT);

            //insert into accounts
            $insertAcc = mysqli_prepare($conn, "INSERT INTO accounts ( email, password, status) VALUES ( ?,?, 'new')");
            mysqli_stmt_bind_param($insertAcc, 'ss', $email, $enc_pass);
            if (!mysqli_stmt_execute($insertAcc)) {
                die('Error: ' . $conn->error);
            } else {
                // get the account id
                $accID = mysqli_insert_id($conn);
                ///insert into employees
                $insertEmp = mysqli_prepare($conn, "INSERT INTO employees ( name, role, status, acc_id, manager_id ) VALUES ( ?,?, 'active', ?,? )");
                mysqli_stmt_bind_param($insertEmp, 'ssis', $EmpName, $role, $accID, $manager);
                if (!mysqli_stmt_execute($insertEmp)) {
                    die('Error: ' . $conn->error);
                } else {
                    // alert successful registration
                    $message = "<b>User successfully registered!</b>";
                    $type = "success";
                    $rtn = true;
                }
            }
        }
        //notify error
        setErrorFlag(myDiv("div", $message), $type);
        return $rtn;
    } else {
        //alert user that email is already in use
        $message = "<b>The email is already in use!</b>";
        setErrorFlag(myDiv("div", $message));
        return $rtn;
    }
}

function handleLogin(string $email, string $password)
{
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    //check if email is valid
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $rtnmess = "Entered email is invalid!";
        setErrorFlag($rtnmess);
        return;
    }
    include '../include/con_db.php';
    @session_start();
    $password = $email . $password;
    //prepare select statement
    $sql = "SELECT  a.password, a.email, a.acc_id, a.status, e.emp_id, e.name, e.role 
            FROM employees e join accounts a
            on e.acc_id = a.acc_id
            where a.email = ?";
    $getUser = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($getUser, 's', $email);
    //set error message
    $type = "fail";
    $message = "<b>Login Failed! Incorrect email or password</b>";
    $errMsg = myDiv("div", $message);
    //search in db
    mysqli_stmt_execute($getUser);
    $result = mysqli_stmt_get_result($getUser);
    $numrows = mysqli_num_rows($result);
    if ($numrows != 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            //store user info    
            $storedpass = $row["password"];
            $storedemail = $row["email"];
            $_SESSION["accid"] =  $row["acc_id"];
            $_SESSION["userStatus"] = $row["status"];
            $_SESSION["emp_id"] = $row["emp_id"];
            $_SESSION["uName"] = $row["name"];
            $_SESSION["role"] = $row["role"];
            $_SESSION["errorFlag"] = null;
        }
        //auth credential
        if ($email == $storedemail && password_verify($password, $storedpass) && $_SESSION["userStatus"] != "deactivated") {
            if ($_SESSION["userStatus"] == "new") {
                echo myGoToPage(posRoot . "home/user/change_pass.php");
            } else echo myGoToPage(posRoot . "home/homepage.php");
        } else {
            //print error
            setErrorFlag($errMsg, $type);
        }
    } else {
        //print error
        setErrorFlag($errMsg);
    }
}


function handleChangePass(int $accID, string $oldPass, string $newPass)
{
    include 'con_db.php';
    $showError = true;
    //get current password
    $sql = "select email, password from accounts where acc_id = ?";
    $getPassword = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($getPassword, 'i', $accID);
    mysqli_stmt_execute($getPassword);
    $result = mysqli_stmt_get_result($getPassword);
    while ($row = mysqli_fetch_assoc($result)) {
        $storedemail = $row["email"];
        $storedpass = $row["password"];
    }
    $currentPass = $storedemail . $oldPass;
    $updatedNewPass = $storedemail . $newPass;
    //verify if password is correct
    if (password_verify($currentPass, $storedpass)) {
        // verify if new password is not same as old password
        if (!password_verify($updatedNewPass, $storedpass)) {
            // Validate password strength
            if (validatePassword($newPass)) {
                //encrypt password
                $hashedNewPass = password_hash($updatedNewPass, PASSWORD_DEFAULT);
                //update password
                $updateSql = "UPDATE accounts SET password= ?, status= 'active' WHERE acc_id = ?";
                $setPassword = mysqli_prepare($conn, $updateSql);
                mysqli_stmt_bind_param($setPassword, 'si', $hashedNewPass, $accID);
                if (!mysqli_stmt_execute($setPassword)) {
                    die('Error: ' . $conn->error);
                } else {
                    $showError = false;
                    $_SESSION["userStatus"] = "active";
                    // alert successful registration
                    $message = myDiv("div", "<b>Password successfully changed!</b>");
                    setErrorFlag($message, "success");
                    echo myGoToPage(posRoot . "home/homepage.php");
                }
            } else {
                $message = myDiv("div", "<b>Password does not satisfy requirements!</b>");
                setErrorFlag($message);
            }
        } else {
            $message = myDiv("div", "<b>Same password cannot be used!</b>");
            setErrorFlag($message);
        }
    } else {
        $message = myDiv("div", "<b>Incorrect password!</b>");
        setErrorFlag($message);
    }
    if ($showError == true) showErrMsg();
}

function handleAddProduct(string $name, string $description, float $price, string $barcode, int $empid)
{
    @include 'con_db.php';
    //check for duplicate barcode
    $bcsql = "select pd_id from products where barcode = ?";
    $getbc = mysqli_prepare($conn, $bcsql);
    mysqli_stmt_bind_param($getbc, 's', $barcode);
    mysqli_stmt_execute($getbc);
    $result = mysqli_stmt_get_result($getbc);
    $numrow = mysqli_num_rows($result);
    if ($numrow == 0) {
        $insertsql = "insert into products (name, description, price, barcode, status, emp_id) values (?,?,?,?,'active',?)";
        $insertPd = mysqli_prepare($conn, $insertsql);
        mysqli_stmt_bind_param($insertPd, 'ssdsi', $name, $description, $price, $barcode, $empid);
        if (!mysqli_stmt_execute($insertPd)) {
            die('Error: ' . $conn->error);
        } else {
            // alert successful registration
            $message = myDiv("div", "<b>Product inserted!</b>");
            setErrorFlag($message, "success");
            $insertid = mysqli_insert_id($conn);
            return $insertid;
        }
    } else {
        $message = myDiv("div", "<b>Barcode is already in the system!</b>");
        setErrorFlag($message, "fail");
        return false;
    }
}
/**
 * @param string $type query type (employee || product)
 */
function handleshowdetails(string $type, int $id): mysqli_result
{
    @include 'con_db.php';
    switch ($type) {
        case "product":
            $sql = "SELECT p.pd_id, p.name, p.description, p.price, p.barcode,p.date, e.name as \"emp\", c.ctg_id , c.name as \"ctg\"
                from products p
                join employees e on e.emp_id = p.emp_id
                join pd_category c on c.ctg_id = p.pd_category 
                where pd_id = ?";
            break;
        case "employee":
            $sql = "SELECT e1.emp_id, e1.name, ac.email, e1.role, e2.name as \"manager\", e1.date 
                from employees e1 
                join employees e2 on e1.manager_id = e2.emp_id 
                join accounts ac on e1.acc_id = ac.acc_id 
                where e1.emp_id = ?";
            break;
        default:
            die("Unauthorized access");
            break;
    }
    $getpd = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($getpd, 'i', $id);
    if (!mysqli_stmt_execute($getpd)) {
        die("Error: " . $conn->error);
    }
    $result = mysqli_stmt_get_result($getpd);
    return $result;
}

function handlechangedetails(string $from) {
    @include 'con_db.php';

    //generate sql
    if ($from == "product") {
        $sql = "update products set ";
        $table = " where pd_id = ?";
    }
    elseif ($from == "employee") {
        $sql = "update employees set ";
        $table = " where emp_id = ?";
    }
    else {
        die("Unauthorized access!");
    }
    
    $type = "";
    $i = 0;
    $values = [];
    extract($_POST);
    foreach ($_POST as $key => $value) {
        if ($i > 0) $sql .= ", ";
        $i++;
        switch ($key) {
            case "name":
                $sql .= "name = ?";
                $type .= "s";
                array_push($values, $name);
                break;
            case "desc":
                $sql .= "description = ?";
                $type .= "s";
                array_push($values, $desc);
                break;
            case "price":
                $sql .= "price = ?";
                $type .= "i";
                array_push($values, $price);
                break;
            case "barcode":
                $sql .= "barcode = ?";
                $type .= "s";
                array_push($values, $barcode);
                break;
            case "category":
                $sql .= "pd_category = ?";
                $type .= "i";
                array_push($values, $category);
                break;
            case "role":
                $sql .= "role = ?";
                $type .= "s";
                array_push($values, $role);
                break;
            default:
                $i--;
                break;
        }
    }
    $sql = substr($sql, 0, -2);
    $sql .= $table;
    array_push($values, $pid);
    $type .= "i";
    //sql generated
    //prepare query
    $updatepd = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($updatepd, $type, ...$values);
    if (!mysqli_stmt_execute($updatepd)){
        die("Error : $conn->error");
    }
    $message = myDiv("div", ucfirst($from) ." changed!");
    setErrorFlag($message, "success");
    showErrMsg();
}

function handleaddcategory($name, $description) {
    @include 'con_db.php';
    //check for duplicate barcode
    $ctgsql = "select ctg_id from pd_category where name = ?";
    $getctg = mysqli_prepare($conn, $ctgsql);
    mysqli_stmt_bind_param($getctg, 's', $name);
    mysqli_stmt_execute($getctg);
    $result = mysqli_stmt_get_result($getctg);
    $numrow = mysqli_num_rows($result);
    if ($numrow == 0) {
        $insertsql = "insert into pd_category (name, description) values (?,?)";
        $insertctg = mysqli_prepare($conn, $insertsql);
        mysqli_stmt_bind_param($insertctg, 'ss', $name, $description);
        if (!mysqli_stmt_execute($insertctg)) {
            die('Error: ' . $conn->error);
        } else {
            // alert successful registration
            $message = myDiv("div", "<b>Category created!</b>");
            setErrorFlag($message, "success");
            $insertid = mysqli_insert_id($conn);
            return $insertid;
        }
    } else {
        $message = myDiv("div", "<b>Category is already in the system!</b>");
        setErrorFlag($message, "fail");
        return false;
    }

}