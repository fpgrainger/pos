// -------  FUNCTIONS  -------
//to check if password is valid and match
function validatePass(pass) {
  var str = pass;
  var match_result = 0;
  var valid_result = 0;
  //check if password fulfill requirement
  if (
    str.match(/[a-z]/g) &&
    str.match(/[A-Z]/g) &&
    str.match(/[0-9]/g) &&
    str.length >= 8
  ) {
    //clear warning
    document.getElementById("msg_validity").style.display = "none";
    valid_result = true;
  } else {
    //show warning
    document.getElementById("msg_validity").innerHTML =
      "Password should have at least <ul><li> 1 uppercase letter </li><li> 8 characters</li><br><li>1 lowercase letter</li><li>1 number</li></ul>";
    valid_result = false;
  }
  return valid_result;
}

function matchPass(pass, confirmPass) {
  match_result = false;
  if (confirmPass.length != 0 && pass.length != 0) {
    //check if password match
    if (pass == confirmPass) {
      //clear warning
      document.getElementById("msg_match").style.display = "none";
      match_result = true;
    } else {
      //show warning
      document.getElementById("msg_match").innerHTML =
        "Password does not match";
    }
  }
  return match_result;
}

function toggleButton(value1 = false, value2 = false) {
  if (value1 && value2)
    document.getElementById("toggleSubmit").disabled = false;
  else document.getElementById("toggleSubmit").disabled = true;
}

function createHiddenInput(name, value) {
    let hiddenInput = document.createElement("input");
    hiddenInput.setAttribute("type", "hidden");
    hiddenInput.setAttribute("name", name);
    hiddenInput.setAttribute("value", value);
    return hiddenInput
}

function changepddetails(iname, idesc, iprice, ibarcode, icategory){
    let formholder = document.createElement("form");
    formholder.setAttribute("method", "POST");
    formholder.setAttribute("action", pageURL);
    if (iname.value.toUpperCase() != pdname) formholder.appendChild(createHiddenInput("name", iname.value));
    if (idesc.value.toUpperCase() != desc) formholder.appendChild(createHiddenInput("desc", idesc.value));
    if (Number(iprice.value) != Number(price)) formholder.appendChild(createHiddenInput("price", iprice.value));
    if (ibarcode.value != barcode) formholder.appendChild(createHiddenInput("barcode", ibarcode.value));
    if (icategory.value != category) formholder.appendChild(createHiddenInput("category", icategory.value));
    if (formholder.length > 0){
        formholder.appendChild(createHiddenInput("change", 1))
        formholder.appendChild(createHiddenInput("pid", pdid))
        document.body.appendChild(formholder);
        formholder.submit();    
    }
}

function changeempdetails(irole) {
  let formholder = document.createElement("form");
  formholder.setAttribute("method", "POST");
  formholder.setAttribute("action", pageURL);
  if (irole.value.toUpperCase() != emprole) formholder.appendChild(createHiddenInput("role", irole.value));
  if (formholder.length > 0){
    formholder.appendChild(createHiddenInput("change", 1))
    formholder.appendChild(createHiddenInput("pid", pdid))
    document.body.appendChild(formholder);
    formholder.submit();    
  }
}

function tablegoto(event, action) {
  let buttonClicked = event.target;
  let parentid = buttonClicked.parentElement.parentElement.id;
  let formholder = document.createElement("form");
  formholder.setAttribute("method", "POST");
  formholder.setAttribute("action", action);
  formholder.appendChild(createHiddenInput("pid", parentid));
  document.body.appendChild(formholder);
  formholder.submit();
}

// -------- EVENT LISTENERS ---------
//console.time("setListener");

document.getElementById("registerForm")?.addEventListener("keyup", function () {
  if (pass?.value) {
    toggleButton(
      validatePass(pass.value),
      matchPass(pass.value, confirm_pass.value)
    );
  }
});

document.getElementById("changePassForm")?.addEventListener("keyup", function () {
    if (newPass?.value) {
      toggleButton(
        validatePass(newPass.value),
        matchPass(newPass.value, confirm_pass.value)
      );
    }
  });

document.getElementById("btnCloseNoti")?.addEventListener("click", function () {
  document.getElementById("notification-bar").remove();
});

//-----------------------initializer--------------------

//if page changepddt is loaded
if (document.getElementById("pddetails")) {
    //get input 
  inputname = document.getElementById("name");
  inputdesc = document.getElementById("desc");
  inputprice = document.getElementById("price");
  inputbarcode = document.getElementById("barcode");
  selectcategory = document.getElementById("category");

    //initialize default value
  inputname.value = pdname;
  inputdesc.value = desc;
  inputprice.value = price;
  inputbarcode.value = barcode;

  //changepddt specific event listener
  document.getElementById("changepddt")?.addEventListener("click", function () {
        changepddetails(inputname, inputdesc, inputprice, inputbarcode, selectcategory)
  });
}

if(document.getElementById("empdetails")) {
  selectrole = document.getElementById("emprole");

  document.getElementById("changeempdt")?.addEventListener("click", function (){
    changeempdetails(selectrole);
  })
}

//datatables
//datatable related listener
//most likely to generate console warning, suppressing warning
try {
  //button event listeners
  if (document.getElementsByClassName("gotopdbtn").length > 0) {
    const elements = document.getElementsByClassName("gotopdbtn");
    Array.from(elements).forEach(function (element) {
      element.addEventListener("click", function (event) {
        tablegoto(event, "/pos/home/product/pddetails.php");
      });
    });
  }

  if (document.getElementsByClassName("gotoempbtn").length > 0) {
    const elements = document.getElementsByClassName("gotoempbtn");
    Array.from(elements).forEach(function (element) {
      element.addEventListener("click", function (event) {
        tablegoto(event, "/pos/home/user/userdetails.php");
      });
    });
  }

//initialize datatable
  $("#emp-list")?.DataTable({
    order: [[2, "desc"]],
  });
  $("#pd-list")?.DataTable({
    order: [[2, "desc"]],
  });
} catch {
  //do nothing
}

//console.timeEnd("setListener");
