<?php
    if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["sid"])) {
        include '../../include/con_db.php';
        include_once 'Products.php';
        //GET SALES
        $salesid=$_GET["sid"];
        $salessql = "select date, grand_total from sales where sale_id=?";
        $getsales = mysqli_prepare($conn, $salessql);
        mysqli_stmt_bind_param($getsales,'i', $salesid);
        if (!mysqli_stmt_execute($getsales)){
            die($conn->error);
        }
        else {
            $result = mysqli_stmt_get_result($getsales);
            $numrows = mysqli_num_rows($result);
            if ($numrows != 0) {
                while ($row = mysqli_fetch_assoc($result)){
                    $date =$row["date"];
                    $total = $row["grand_total"];
                }
            }
            mysqli_stmt_close($getsales);
            //GET SALES DETAILS
            $detailsql = "SELECT p.pd_id, p.name, unit_price, quantity FROM sales_details s join products p on p.pd_id = s.pd_id WHERE sales_id=$salesid";
            //PRODUCTS HOLDER
            $itemsarr=[];
            $result = mysqli_query($conn, $detailsql);
            while($pdrow = mysqli_fetch_assoc($result)) {
                $product = new Products($pdrow["pd_id"], $pdrow["quantity"]);
                $product->setprice($pdrow["unit_price"]);
                $product->setname($pdrow["name"]);
                array_push($itemsarr, $product);
            }
        }
    }
    else {
        die("error");
    }
?>
<html>
<head>SALES ID - <?php echo $salesid;?>
</head>
<body>
<div style="text-align:right;">
        <b>KOPERASI SAMTTAJ</b> 
    </div>
    <div style="text-align: left;border-top:1px solid #000;">
        <div style="font-size: 24px;color: #666;">RECEIPT</div>
    </div>
<table style="line-height: 1.5;">
    <tr>
        <td><b>Date:</b> <?php echo $date; ?></td>
    </tr>
</table>

<div></div>
    <div style="border-bottom:1px solid #000;">
        <table style="line-height: 2;">
            <tr style="font-weight: bold;border:1px solid #cccccc;background-color:#f2f2f2;">
                <td style="border:1px solid #cccccc;width:200px;">Item Name</td>
                <td style = "text-align:right;border:1px solid #cccccc;width:85px">Price (RM)</td>
                <td style = "text-align:right;border:1px solid #cccccc;width:75px;">Quantity</td>
                <td style = "text-align:right;border:1px solid #cccccc;">Subtotal (RM)</td>
            </tr>
<?php
foreach ($itemsarr as $item) {
    ?>
    <tr> <td style="border:1px solid #cccccc;"><?php echo $item->name; ?></td>
                    <td style = "text-align:right; border:1px solid #cccccc;"><?php echo number_format((float)$item->price, 2); ?></td>
                    <td style = "text-align:right; border:1px solid #cccccc;"><?php echo $item->quantity; ?></td>
                    <td style = "text-align:right; border:1px solid #cccccc;"><?php echo number_format((float)$item->getTotal(), 2); ?></td>
               </tr>
<?php
}
?>
<tr style = "font-weight: bold;">
    <td></td><td></td>
    <td style = "text-align:right;">Total (RM)</td>
    <td style = "text-align:right;"><?php echo number_format($total, 2); ?></td>
</tr>
</table></div>
</body>
</html>

