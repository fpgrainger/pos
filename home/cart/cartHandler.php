<?php 
    include '../../include/con_db.php';
    @session_start();
    include_once 'Products.php';

    $response = array("status" => "error");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $json = file_get_contents('php://input');
        //$json = '[{"id":1,"quantity":1},{"id":12,"quantity":1},{"id":13,"quantity":1}]';
        $data = json_decode($json);
        //get sales id
        $sqlsales = "insert into sales (grand_total, emp_id) values (0,?)";
        $getsalesid = mysqli_prepare($conn, $sqlsales);
        mysqli_stmt_bind_param($getsalesid, 'i', $_SESSION["emp_id"]);
        mysqli_stmt_execute($getsalesid);
        $salesid = mysqli_insert_id($conn);
        mysqli_stmt_close($getsalesid);
        //get price and count total then insert into db
        $sqlgetprice = "select price from products where pd_id = ?";
        $sqlinsertsales = "insert into sales_details (pd_id, sales_id, unit_price, quantity) values (?,$salesid,?,?)";
        $grandTotal = 0;
        $pdarr = [];
        //prepare statement
        $getprice = mysqli_prepare($conn, $sqlgetprice);
        $insertsales = mysqli_prepare($conn, $sqlinsertsales);
        $i=0;
        foreach ($data as $data) {
            $pdarr[$i] = new Products($data->id, $data->quantity);
            if (!$pdarr[$i]->fetchprice($getprice)) {
                die(json_encode($response));
            }
            $grandTotal += $pdarr[$i]->getTotal();
            if(!$pdarr[$i]->insertProduct($insertsales)){
                die(json_encode($response));
            }
            $i++;
        }
        mysqli_stmt_close($getprice);
        mysqli_stmt_close($insertsales);

        //update grand total in sales db
        $sqlupdatesales = "update sales set grand_total= ? where sale_id = ?";
        $setgrandtotal = mysqli_prepare($conn, $sqlupdatesales);
        mysqli_stmt_bind_param($setgrandtotal,'di', $grandTotal, $salesid);
        mysqli_stmt_execute($setgrandtotal);
        mysqli_stmt_close($setgrandtotal);
        $response = ["status" => "success", "salesid" => $salesid];
    }
    echo json_encode($response);
?>