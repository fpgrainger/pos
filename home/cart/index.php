<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cart</title>
    <?php
    include '../../include/myFunction.php';
    autoInclude();
    myCheckSession();
    ?>
    <script src="cart.js" defer></script>
</head>

<body>
    <div class="body-wrapper">
        <div id="notification-bar"></div>
        <h2>Order Cart</h2>
        <label for="barcode-input">Add to cart</label>
        <input type="number" id="barcode-input" name="barcode-input" step="1" placeholder="Enter barcode">
        <button id="add-button">Add</button>
        <br>
        <div id="empty-cart">
            <h3>Cart is empty. Please input barcode.</h3>
        </div>
        <div id="cart-display">
            <fieldset>
                <legend>
                    <h3>Item List</h3>
                </legend>
                <table class="cart-items" id="cart-list">
                    <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Name</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <th colspan="3">Grand Total</th>
                        <th id="cart-total">RM 0.00</th>
                    </tfoot>
                </table>
            </fieldset>
            <button id="pop-checkout">Checkout</button>
            <!-- <button id="checkout">Checkout</button> -->
        </div>
        <!-- The Modal -->
        <div id="checkout-modal" class="checkout-bg">
            <!-- Modal content -->
            <div class="checkout-content">
                <div class="checkout-header">
                    <span class="checkout-close">&times;</span>
                    <h2>Checkout</h2>
                </div>
                <div class="checkout-body">
                    <table>
                        <tr>
                            <th>Grand Total</th>
                            <th id="checkout-grand-total">0</th>
                        </tr>
                        <tr>
                            <td><label for="payment">Paid Amount (RM)</label></td>
                            <td><input type="number" id="paid" step="0.01"></td>
                        </tr>
                        <tfoot id="change-wrapper">
                            <th>Change</th>
                            <th id="change"></th>
                        </tfoot>
                    </table>
                </div>
                <div class="checkout-footer">
                    <span><button id="checkout">Checkout</button></span>
                </div>
            </div>

        </div>
    </div>
</body>

</html>