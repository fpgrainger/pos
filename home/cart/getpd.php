<?php
    include '../../include/con_db.php';
    $return_arr = array();
    // $barcode = "9556641320027";
    $barcode = $_REQUEST['barcode'];
    $sql = "SELECT pd_id, name, price, barcode FROM products WHERE barcode =?";
    $getproduct = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($getproduct,'s', $barcode);
    mysqli_stmt_execute($getproduct);
    $result = mysqli_stmt_get_result($getproduct);
    $numrows = mysqli_num_rows($result);
    if ($numrows !=0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row["pd_id"];
            $pdname = strtoupper($row["name"]);
            $price = $row["price"];
            $barcode = $row["barcode"];
        }   
    }
    else {
        $id = $pdname = $price = $barcode = null;
    }
    //encode array to json
    $return_arr = array("id" => $id, "pdname" => $pdname, "price" => $price, "barcode" => $barcode);
    echo json_encode($return_arr);
?>