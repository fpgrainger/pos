class Products {
    constructor(pdid, name, price, bc) {
        this.id = pdid
        this.name = name
        this.price = price
        this.barcode = bc
        this.quantity = 1
    }
    setQty (qty) {
        this.quantity = qty
    }
    increaseQty() {
        this.quantity++
    }
}

//global var to store cartitem
const cartItems = []

async function fetchProduct(barcode) {
    const response = await fetch('getpd.php?barcode='+barcode)
    const obj = await response.json()
    //display product
    if (obj.id == null) {
        alert("Barcode not found!")
        return
    }
    var currentItem = new Products(obj.id, obj.pdname, obj.price, obj.barcode)
    cartItems.push(currentItem)
    addItemToCart(currentItem)
    checkEmptyCart()
}
/**
 * 
 * @param {string} type expected value: id , name , price , barcode
 * @param {mixed} value 
 * @returns 
 */
function getProductIndex(type, value) {
    var index = 0
    switch(type) {
        case "id": 
            while (!(cartItems[index].id == value)) index++
            break
        case "name":
            while (!(cartItems[index].name == value)) index++
            break
        case "price":
            while (!(cartItems[index].price == value)) index++
            break
        case "barcode":
            while (!(cartItems[index].barcode == value)) index++
            break
        default: return false
    }
    return index
}

function calcCartTotal () {
    let total = 0
    for (let i = 0; i < cartItems.length; i++) {
        total += (cartItems[i].price * cartItems[i].quantity)
    }
    let totalnumber = total
    total = total.toFixed(2)
    document.getElementById("cart-total").innerHTML = "RM " + total
    return totalnumber
}

function checkEmptyCart() {
    if (cartItems.length==0) {
        document.getElementById("empty-cart").style.display = "inline-block"
        document.getElementById("cart-display").style.display = "none"
    }
    else {
        document.getElementById("empty-cart").style.display = "none"
        document.getElementById("cart-display").style.display = "inline-block"
    }
}


function checkBarcode (product){
    return product.barcode == document.getElementById("barcode-input").value
}
function removeCartItem(event) {
    var buttonClicked = event.target
    var pdid = buttonClicked.parentElement.parentElement.id
    var idx = getProductIndex("id", pdid)
    cartItems.splice(idx,1)
    buttonClicked.parentElement.parentElement.remove()
    calcCartTotal()
    checkEmptyCart()
}

function quantityChanged(event) {
    var input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    var pdid = input.parentElement.parentElement.id
    var idx = getProductIndex("id", pdid)
    cartItems[idx].quantity = input.value
    calcCartTotal()
}

function addItemToCart(currentItem) {
    var cartRow = document.createElement('tr');
    cartRow.classList.add('cart-row');
    cartRow.setAttribute('id', currentItem.id);
    var cartDisplay = document.getElementById("cart-list").children[1];
    var cartRowContents = `
        <td class="pdBarcode" >${currentItem.barcode}</td>
        <td> ${currentItem.name} </td>
        <td>RM ${currentItem.price}</td>
        <td><input class="cart-quantity-input" type="number" value="${currentItem.quantity}" step="1"></td>
        <td><button class="btn-danger" type="button">REMOVE</button></td>`;
    cartRow.innerHTML = cartRowContents;
    cartDisplay.append(cartRow);
    cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeCartItem);
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('keyup', quantityChanged);
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged);
    calcCartTotal()
}

function nextItem(){
    document.getElementById("barcode-input").value=""
    document.getElementById("barcode-input").focus()

}

function handleSearchBarcode() {
    var barcode = document.getElementById("barcode-input").value;
    //find duplicate barcode 
    if (cartItems.find(checkBarcode)) {
        //add quantity of duplicate item
        var idx = getProductIndex("barcode", barcode)
        cartItems[idx].increaseQty();
        document.getElementById(cartItems[idx].id).children[3].children[0].value = cartItems[idx].quantity
        calcCartTotal()
        nextItem()
        return;
    }
    //fetch data 
    fetchProduct(barcode)
    checkEmptyCart()
    nextItem()
}


async function submitCart() {
    const data = []
    for (let index = 0; index < cartItems.length; index++) {
        var holder = {
            id: cartItems[index].id,
            quantity: cartItems[index].quantity
        }
        data[index] = holder
    }
    const options ={
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            "Content-Type" : "application/json"
        }
    }
    const coutresponse = await fetch('cartHandler.php', options)
    const cout = await coutresponse.json()
    if (cout.status == "success") {
        let receiptlink = "http://localhost:8000/pos/home/cart/receipt-template.php?sid="+cout.salesid

        let notiholder = document.getElementById("notification-bar")
        if (notiholder.classList == "") notiholder.classList = "alert success receipt-modal"
        //notification text wrapper
        let msgwrapper = document.createElement("div")
        msgwrapper.style.display = "inline-block"
        //notificaiton text holder
        let msgholder = document.createElement("div")
        msgholder.textContent = "View receipt "
        //receipt link
        let dest = document.createElement("a")
        dest.href = receiptlink
        dest.textContent = "HERE"
        dest.id = "receipt"
        dest.style.color = "yellow"
        //append text
        msgholder.append(dest)
        msgwrapper.appendChild(msgholder)

        //notification close btn
        let btnwrapper = document.createElement("div")
        btnwrapper.className = "btnWrapper"
        let closebtn = document.createElement("button")
        closebtn.className = "btn"
        closebtn.id = "btnCloseNoti"
        closebtn.textContent = "X"
        btnwrapper.append(closebtn)
        //show button and text
        notiholder.append(btnwrapper)
        notiholder.append(msgwrapper)

        //add event listeners 
        //to link
        document.getElementById("receipt").addEventListener("mouseup", function (event) {
            if (event.button == 1){
                document.getElementById("btnCloseNoti").click()
                window.open(receiptlink)
            }
            //--------------------------WIP----------------------
            //prevent leave page if canceled
            if (event.button == 0) {
                if (!confirm("Do you want to leave the page?")) {
                    event.preventDefault()
                }
            }
        })
        //to button
        document.getElementById("btnCloseNoti").addEventListener("click", function () {
            const notibar = document.getElementById("notification-bar")
            notibar.innerHTML=""
            notibar.style.display = "none"
        });
        
        //empty cart
        cartItems.length=0
        document.getElementById("cart-list").children[1].innerHTML = ""
        calcCartTotal()
        checkEmptyCart()
        nextItem()
    }
    //if fail status is received

}
//------cart specific event listeners--------
document.getElementById("barcode-input").addEventListener("keypress", function (event) {
    if (event.key === "Enter" && document.getElementById("barcode-input").value.length>5) {
        event.preventDefault()
        document.getElementById("add-button").click()
    }
})
document.getElementById("add-button").addEventListener("click", function () {
    handleSearchBarcode()
})

document.getElementById("checkout").addEventListener("click", function(){
    let paid = Number(document.getElementById("paid").value)
    let checkouttotal = calcCartTotal()
    if(paid > checkouttotal){
        let change = paid - checkouttotal
        change = change.toFixed(2)
        document.getElementById("change").innerHTML = "RM " + change
        document.getElementById("change-wrapper").style.display = ""
        submitCart()
        document.getElementById("checkout").style.display = "none"
    }
    else if (isNaN(paid)){
        alert("Invalid input")
    }
    else {
        alert("Insufficient payment")
    }
})

//autofire on startup
//hide empty cart on start up
checkEmptyCart()

// Get the modal
var bg = document.getElementById("checkout-modal");

document.getElementById("pop-checkout").addEventListener("click", function(){
    document.getElementById("checkout-grand-total").innerHTML = "RM " + calcCartTotal().toFixed(2)
    document.getElementById("checkout").style =""
    document.getElementById("change-wrapper").style.display = "none" 
    bg.style.display = "block";
    document.getElementById("paid").focus()
});

document.getElementsByClassName("checkout-close")[0].addEventListener("click", function(){
    bg.style.display = "none";
    document.getElementById("paid").value = ""
    document.getElementById("barcode-input").focus()
});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == bg) {
        document.getElementsByClassName("checkout-close")[0].click()
  }
}

