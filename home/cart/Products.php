<?php 
    class Products {
        private  $id;
        private  $name;
        private  $price;
        private  $quantity;
        private  $total;
        function __construct( $id, $quantity)
        {
            $this->id = $id;
            $this->quantity = $quantity;
        }

        function setname(string $name){
            $this->name= $name;
        }

        function setprice(float $price){
            $this->price = $price;
            $this->total = $price * $this->quantity;
        }

        function fetchprice(mysqli_stmt $prepare) {
            mysqli_stmt_bind_param($prepare,'i',$this->id);
            if (!mysqli_stmt_execute($prepare)) {
                return false;
            }
            $result = mysqli_stmt_get_result($prepare);
            $row = mysqli_fetch_assoc($result);
            $this->setprice($row["price"]);
            return true;
        }

        function insertProduct(mysqli_stmt $prepare){
            mysqli_stmt_bind_param($prepare,'idi', $this->id, $this->price, $this->quantity);
            if (!mysqli_stmt_execute($prepare)) {
                return false;
            }
            else return true;  
        }

        function getTotal() {
            return $this->total;
        }

        function __get($att)
        {
            if (property_exists($this, $att)) {
                return $this->$att;
            }
        }
    }  
?>