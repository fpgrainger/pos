<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Employee</title>
    <?php
        include '../../include/myFunction.php';
        autoInclude();
        myCheckSession(["principal", "manager"]);
        //process POST
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = strtolower( checkInput($_POST["name"]));
            $email = checkInput($_POST["email"]);
            $password = checkInput(($_POST["pass"]));
            $role = "employee";
            if ($_SESSION["role"] == "principal") {
                if ($_POST["role"] == "manager" || $_POST["role"] == "principal") 
                    $role = $_POST["role"];
            }
            if (!handleRegister($name, $email, $password, $role, $_SESSION["emp_id"])){
                showErrMsg();
            }
            else {
                echo myGoToPage(posRoot . "home/homepage.php");
            }
        }
    ?>
</head>
<body>
    <nav><?php include '../../include/nav.php'?></nav>
    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="registerForm">
        <fieldset>
            <legend><h2>Register Employee</h2></legend>
            <div class="input-wrapper">
                <div>
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" required>
                </div>
                <div>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" required>
                </div>
                <div>
                    <label for="pass">Password</label>
                    <input type="password" name="pass" id="pass" required >
                </div>
                <div>
                    <label for="confirm_pass">Confirm Password</label>
                    <input type="password" id="confirm_pass" required >
                </div>
                
                <?php 
                if ($_SESSION["role"] == "principal") {
                    //generate label
                    $label = myDiv("label", "Access Level", ["for" => "role"],"padding-right: 1em");
                    //generate option
                    $rValue = ["employee", "manager", "principal"];
                    $radio = genRadioOption($rValue, "role", "radio-group", 1);
                    $rOption = myDiv("label", $radio, ["id" => "role"]);
                    //generate div to hold label and option
                    echo myDiv("div", $label.$rOption);
                }
                else echo myDiv("input",null, ["type" => "hidden", "name" => "role", "value" => "employee"])
                ?>

                <span class="user-error"  id='msg_validity'></span> <br>
                <span class="user-error" id='msg_match'></span> <br>
                <div id="submit-reset">
                    <button type="reset">Reset</button>
                    <button type="submit" id="toggleSubmit"  >Submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</body>
</html>