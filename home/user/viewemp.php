<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employees List</title>
    <!-- load datables and its stylesheet -->
    <link href="/pos/include/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="/pos/node_modules/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script defer src="/pos/node_modules/vendor/jquery/jquery.min.js"></script>
    <script defer src="/pos/node_modules/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script defer src="/pos/node_modules/vendor/datatables/jquery.dataTables.min.js"></script>
    <script defer src="/pos/node_modules/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <?php
    include '../../include/myFunction.php';
    autoInclude();
    myCheckSession(["principal", "manager"]);
    ?>
    <script></script>
</head>

<body id="page-top">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <div class="my-datatables-wrapper">
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h3 class="m-0 font-weight-bold text-primary">Employees List</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table id="emp-list" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            handleShowEmployeeList();
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>