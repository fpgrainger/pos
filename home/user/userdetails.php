<?php
    include '../../include/myFunction.php';
    myCheckSession(["principal","manager"]);
    cleanPOSTData();
    if (isset($_POST["change"]) && $_POST["change"] == 1) {
        unset($_POST["change"]);
        handlechangedetails("employee");
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST" && strlen($_POST["pid"]) > 0) {
        $result = handleshowdetails("employee", $_POST["pid"]);
        if (mysqli_num_rows($result) == 0) {
            die("Unauthorized access");
        }
        $row = mysqli_fetch_assoc($result);


?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Employee Details</title>
        <?php autoInclude();?>
        <script>
            let pageURL = "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>";
            let pdid = "<?php echo $row["emp_id"] ?>";
            let emprole = "<?php echo strtoupper($row["role"]) ?>";
        </script>
    </head>

    <body>
        <h2>Employee Details</h2>
        <table id="empdetails">
            <tr>
                <td><label for="email">Email</label></td>
                <td><label id="email"><?php echo htmlentities($row["email"]); ?></label></td>
            </tr>
            <tr>
                <td><label for="name">Name</label></td>
                <td><label id="name"><?php echo strtoupper($row["name"]); ?></label></td>
            </tr>
            <?php  
            //show user role to manager
            if ($_SESSION["role"] == "manager"){?>
            <tr>
                <td><label for="role">Role</label></td>
                <td><label id="role"><?php echo strtoupper($row["role"]); ?></label></td>
            </tr>
            <?php } 
            //allow change role for principal
            elseif ($_SESSION["role"] == "principal"){ 
                $allowedroles = ["manager", "employee"];
                $needle = array_search($row["role"], $allowedroles);
                if ($needle !== false){
                    unset($allowedroles[$needle]);
                }    
            ?>
            <tr>
                <td><label for="role">Role</label></td>
                <td><select id="emprole">
                    <option value="<?php echo strtoupper($row["role"]) ?>" selected><?php echo strtoupper($row["role"]) ?></option>
                    <?php 
                        foreach($allowedroles as $value) { ?>
                            <option value="<?php echo strtoupper($value) ?>" ><?php echo strtoupper($value) ?></option>

                     <?php   }}
                    ?>
                </select></td>
            </tr>
            <tr>
                <td><label for="manager">Manager</label></td>
                <td><label id="manager"><?php echo strtoupper($row["manager"]); ?></label></td>
            </tr>
            <tr>
                <td><label for="date">Date Created</label></td>
                <td><label id="manager"><?php echo $row["date"]; ?></label></td>
            </tr>
            <tr>
            <?php 
            //set delete button
            $deletebtn = "<td><button id=\"dltempbtn\">Delete</button><button onclick=\"location.href='viewemp.php'\">Back</button></td>";
            //button for principal
            if ($_SESSION["role"] == "principal"){ 
            ?>
                <td><button id="changeempdt">Change role</button></td>
                <?php echo $deletebtn;?>
                <?php }
                //button for manager
                elseif ($_SESSION["role"] == "manager" && $row["role"] == "employee"){ 
                    echo $deletebtn;}
                    ?>
<td></td>
</tr>
        </table>
    <?php } else {
    die("Unauthorized access!");
}

    ?>
    </body>

    </html>