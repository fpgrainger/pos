<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Change Password</title>
    <?php 
        include '../../include/myFunction.php';
        autoInclude();
        myCheckSession();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $oldPass = $newPass = null;
                $oldPass = checkInput($_POST["oldPass"]);
                $newPass = checkInput($_POST["newPass"]);
                handleChangePass($_SESSION["accid"], $oldPass, $newPass);
        }
    ?>
</head>
<body>
    <?php 
        if ($_SESSION["userStatus"] == "new") {
            echo myDiv("div","<b>You must change your password before logging on the first time. Please update your password.</b>", ["class"=> "alert fail"]);
        }
    ?>
    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" id="changePassForm">
        <fieldset>
            <legend><h2>Change Password</h2></legend>
            <div class="inputWrapper" >
                <div>
                    <label for="oldPass">Old Password</label>
                    <input type="password" name="oldPass" id="oldPass" required >
                </div>
                <div>
                    <label for="newPass">New Password</label>
                    <input type="password" name="newPass" id="newPass" required >
                </div>
                <div>
                    <label for="confirm_pass">Confirm Password</label>
                    <input type="password" id="confirm_pass" required >
                </div>
                <span class="user-error" id='msg_validity'></span> <br>
                <span class="user-error" id='msg_match'></span> <br>
                <div id="submit-reset">
                    <button type="reset">Reset</button>
                    <button type="submit" id="toggleSubmit" disabled >Submit</button>
                </div>            
            </div>
        </fieldset>
    </form>
</body>
</html>