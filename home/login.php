<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <?php 
        include '../include/myFunction.php';
        autoInclude();
    ?>
</head>
<body>
    <?php 
        showErrMsg();
        $email = $pass = "";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $email = $password = null;
            $email = checkInput($_POST["email"]);
            $password = checkInput(($_POST["pass"]));
            handleLogin($email, $password);
        }
        showErrMsg();
    ?>

    <br>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
        <div class="pgCenter">
        <img src="/pos/images/samttajLogo.png" alt="School Logo" style="margin-left: 45px; height: 180px">
            <fieldset>
                <legend><h2>Login</h2></legend>
                <div class="inputWrapper">
                    <div>
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="login" required >
                    </div>
                    <div>
                        <label for="pass">Password</label>
                        <input type="password" name="pass" id="pass" class="login" required>
                    </div> 
                </div>
                <div style="padding-left: 35%;">
                    <button type="submit">Submit</button>
                </div>
            </fieldset>
        </div>
    </form>
</body>
</html>