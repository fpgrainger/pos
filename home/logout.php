<?php 
@session_start();
session_destroy();
?>
<body>
    <form action="../index.php" method="post" id="logout">
        <input type="hidden" name="message" value="<b>You have been logged out.</b>">
    </form>
    <script>            
        document.addEventListener("DOMContentLoaded", function(event) {
        document.createElement('form').submit.call(document.getElementById('logout'));
        });         
</script>
</body>