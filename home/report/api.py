from cProfile import label
import con_db as pos
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time

#get data from db
conn =pos.connect()
query = ("select p.name, count(s.pd_id) as count from sales_details s JOIN products p ON p.pd_id = s.pd_id group by p.pd_id")
#put data into df, sort descending
df = pd.read_sql(query, conn)
df = df.sort_values(["count"], ascending=0).reset_index(drop=True)
#plot only top 5 count
df2 =df.head(5)
ax =sns.barplot(data=df2, x='count', y='name')
ax.set(yticklabels=[])
plt.legend(df2["name"])
#set output file name
savepath = "images/"
savetype = ".png"
filename = savepath + "top5soldproduct" + savetype
plt.savefig(filename, dpi=300)

#send image path to php
posroot = "/pos/home/report/"
showpath = posroot + filename
print (showpath)