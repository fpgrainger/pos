<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homepage</title>
    <?php
    include '../include/myFunction.php';
    autoInclude();
    myCheckSession();
    ?>
</head>

<body>
    <nav><?php include '../include/nav.php'; ?></nav>
    <br>
    <div style="margin-left: 3rem;">
        <h2>Welcome to POS System</h2>
        <?php echo "User: " . strtoupper($_SESSION["uName"]) . "<br>Role: " . strtoupper($_SESSION["role"]); ?>
        <fieldset>
            <legend>Main Menu</legend> <br>
            <div>
                <?php
                switch ($_SESSION["role"]) {
                    case 'principal':
                        $location = ["View Employee" => posRoot . "home/user/viewemp.php", "Add Employee" => posRoot . "home/user/register.php", "Appoint Manager" => "index.php", "View Report" => "index.php"];
                        break;
                    case 'manager':
                        $location = ["View Employee" => posRoot . "home/user/viewemp.php", "Add Employee" => posRoot . "/home/user/register.php", "View Product" => posRoot . "home/product/viewpd.php", "Add Product" => posRoot . "home/product/addproduct.php", "View Report" => "index.php"];
                        break;
                    case 'employee':
                        $location = ["Go to Cart" => posRoot . "home/cart/index.php"];
                        break;
                }

                foreach ($location as $key => $value) {
                    $otherAtt = ["onclick" => "location.href='$value'"];
                    echo myDiv("button", $key, $otherAtt);
                    echo "<br>";
                }
                ?>
                <button onclick="location.href='<?php echo posRoot . "home/user/change_pass.php"; ?>'">Change Password</button>
            </div> <br>
        </fieldset>
    </div>
</body>

</html>