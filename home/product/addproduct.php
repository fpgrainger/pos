<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <?php
        @session_start();    
        include '../../include/myFunction.php';
        autoInclude();
        myCheckSession(["principal", "manager"]);
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = strtolower( checkInput($_POST["name"]));
            $description = strtolower( checkInput($_POST["desc"]));
            $price = checkInput($_POST["price"]);
            $barcode = checkInput($_POST["barcode"]);
            $inspd = handleAddProduct($name, $description, $price, $barcode, $_SESSION["emp_id"]);
            if($inspd !== false){
                if (($_FILES["file"]["error"] ==0)) {
                    uploadImage(basename($_FILES["file"]["name"]), $_FILES["file"]["tmp_name"], $inspd);
                }
            }
            showErrMsg();
        }
    ?>
</head>
<body>
    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST" id="addproductform" enctype="multipart/form-data">
        <fieldset>
            <legend><h2>New Product</h2></legend>
            <div class="input-wrapper">
                <div>
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" required>
                </div>
                <div>
                    <label for="desc">Description</label>
                    <input type="text" id="desc" name="desc" required>
                </div>
                <div>
                    <label for="price">Price</label>
                    <input type="number" name="price" id="price" step=".01" required>
                </div>
                <div>
                    <label for="barcode">barcode</label>
                    <input type="number" name="barcode" id="barcode" required>
                </div>
                <div>
                    <label for="file">Upload Image:</label>
                    <input type="file" id="file" name="file">
                </div>
                <div id="submit-reset">
                    <button type="reset">Reset</button>
                    <button type="submit">Submit</button>
                </div>
            </div>
        </fieldset>
    </form>
</body>
</html>