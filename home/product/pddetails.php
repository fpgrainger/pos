<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include '../../include/myFunction.php';
    @include '../../include/con_db.php';
    cleanPOSTData();
    if (isset($_POST["change"]) && $_POST["change"] == 1) {
        unset($_POST["change"]);
        handlechangedetails("product");
    }
    //show details
    if (isset($_POST["pid"])) {
        $result = handleshowdetails("product", $_POST["pid"]);
        if (mysqli_num_rows($result) == 0) {
            die("Product not found!");
        }
        $row = mysqli_fetch_assoc($result);

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Product Details</title>
        <?php autoInclude() ?>
        <script>
            let pageURL = "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"
            let pdid = "<?php echo $row["pd_id"]; ?>"
            let pdname = "<?php echo strtoupper($row["name"]); ?>";
            let desc = "<?php echo strtoupper($row["description"]); ?>";
            let price = "<?php echo $row["price"]; ?>";
            let barcode = "<?php echo $row["barcode"]; ?>";
            let category = "<?php echo $row["ctg_id"]; ?>";
        </script>
    </head>

    <body>
        <h2>Product Details</h2>
        <table id="pddetails">
            <tr>
                <td><label for="name">Name</label></td>
                <td><input type="text" id="name"></td>
            </tr>
            <tr>
                <td><label for="desc">Description</label></td>
                <td><input type="text"  id="desc"></td>
            </tr>
            <tr>
                <td><label for="price">Price (RM)</label></td>
                <td><input type="number"  id="price" step="0.01"></td>
            </tr>
            <tr>
                <td><label for="barcode">Barcode</label></td>
                <td><input type="text"  id="barcode"></td>
            </tr>
            <tr>
                <td><label for="category">Category</label></td>
                <td>
                    <select id="category">
                        <option value="<?php echo $row["ctg_id"]; ?>">
                            <?php echo strtoupper($row["ctg"]); ?>
                        </option>
                        <?php 
                            $sql = "SELECT ctg_id, name FROM pd_category WHERE ctg_id != ?; ";
                            $getoption = mysqli_prepare($conn, $sql);
                            mysqli_stmt_bind_param($getoption, 'i', $row["ctg_id"]);
                            if(!mysqli_stmt_execute($getoption)){
                                die ("Error : " . $conn->error);
                            }
                            echo genSelectOpt(mysqli_stmt_get_result($getoption), "ctg_id", "name");
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="date">Date Created</label></td>
                <td><label id="date"><?php echo $row["date"]; ?></label></td>
            </tr>
            <tr>
                <td><label for="emp"> Created by</label></td>
                <td><label id="emp"><?php echo strtoupper($row["emp"]); ?></label></td>
            </tr>
            <tr>
                <td><button id="changepddt">Save Changes</button></td>
                <td><button onclick="location.href='viewpd.php'">Back</button></td>
            </tr>
        </table>
        <br>
        <?php }} else {
    die("Unauthorized access!");
}

    ?>
    </body>

    </html>